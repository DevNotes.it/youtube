#!/bin/bash

# Define the array of possible log lines
log_lines=(
"[INFO] Starting application version 1.2.3"
"[DEBUG] Loading configuration file"
"[INFO] Successfully loaded configuration"
"[INFO] Connecting to database"
"[ERROR] Failed to connect to database: Connection timeout"
"[INFO] Retrying database connection"
"[INFO] Successfully connected to database"
"[INFO] Application running"
)

start_date=$(date -d "2023-02-01" +%s)
end_date=$(date -d "2023-03-01" +%s)
diff_days=$(( (end_date - start_date) / (60*60*24) ))

# Generate 5 unique random days
declare -A random_days
while [ ${#random_days[@]} -lt 5 ]; do
    random_day=$(( RANDOM % diff_days ))
    random_date=$(date -d "@$(($start_date + $random_day*60*60*24))" +%Y%m%d)
    random_days[$random_date]=1
done

# For each selected day, generate 4 unique random hours
for date in "${!random_days[@]}"; do
    declare -A random_hours
    while [ ${#random_hours[@]} -lt 4 ]; do
        random_hour=$(printf "%02d" $(( RANDOM % 24 )))
        random_hours[$random_hour]=1
    done

    # For each hour, generate a log file
    for hour in "${!random_hours[@]}"; do
        # Random minute and second for the log file timestamp
        random_minute=$(printf "%02d" $((RANDOM % 60)))
        random_second=$(printf "%02d" $((RANDOM % 60)))

        # Filename based on the date and hour
        filename="log_file_${date}_${hour}${random_minute}.${random_second}.log"

        # Shuffle log_lines array and pick the first $((RANDOM % ${#log_lines[@]} + 1)) lines
        mapfile -t shuffled_log_lines < <(printf "%s\n" "${log_lines[@]}" | shuf)
        selected_log_lines=("${shuffled_log_lines[@]:0:$((RANDOM % ${#log_lines[@]} + 1))}")

        # Write selected log lines to file
        for log_line in "${selected_log_lines[@]}"; do
            echo "${date} ${hour}:${random_minute}:${random_second} $log_line" >> $filename
        done

        # Change the file's timestamp
        touch -t "${date}${hour}${random_minute}.${random_second}" $filename
    done

done
